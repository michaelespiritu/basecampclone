<?php

namespace Tests\Feature;

use Carbon\Carbon;
use App\Model\Task;
use App\Model\Todo;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_add_task()
    {
        $this->withExceptionHandling();
        $todo = factory(Todo::class)->create();

        $user = $this->signIn($todo->project->owner);

        $task = $this->post($todo->path() . '/task', $attr = [
            'tasks' => [
                [
                    'details' => 'Task 1'
                ],
                [
                    'details' => 'Task 2'
                ],
            ]
        ]);

        $task->assertStatus(201);
        $this->assertDatabaseHas('tasks', [
            'task' => 'Task 1'
        ]);
        $this->assertDatabaseHas('tasks', [
            'task' => 'Task 2'
        ]);
    }

    /** @test */
    public function a_user_can_tick_done()
    {
        $this->withExceptionHandling();
        $task = factory(Task::class)->create();

        $user = $this->signIn($task->todo->project->owner);

        $update = $this->post($task->path() . '/update/done', $attr = [
            'done' => true
        ]);

        $update->assertStatus(200);

        $this->assertDatabaseHas('tasks', [
            'task' => $task->task,
            'done_by' => $user->id,
            'done' => Carbon::now()
        ]);
    }
}
