<?php

namespace Tests\Feature;

use App\User;
use App\Model\Todo;
use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TodoMemberTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function user_can_add_its_employee_to_todo () 
    {
        $project= $this->companyWithEmployeeAndProjectSignIn();

        $todo = factory(Todo::class)->create([
            'project_id' => $project->owner->id
        ]);

        $employees = $project->employees;

        $invited = $this->post($todo->path()."/invite", [
            'value' => true,
            'email' => $employees[1]['email']
        ]);
        $invited->assertStatus(200);

        $this->assertTrue($todo->employees->contains($employees[1]));
    }

    /** @test */
    function owner_cannot_add_same_member_to_todo () 
    {
        $this->withExceptionHandling();

        $project = $this->companyWithEmployeeAndProjectSignIn();

        $todo = factory(Todo::class)->create([
            'project_id' => $project->owner->id
        ]);

        $employees = $project->employees;

        $todo->employees()->attach($employees[0], ['identifier' => Str::uuid()]);
        
        $invited = $this->post($todo->path()."/invite", [
            'value' => true,
            'email' => $employees[0]->email
        ]);

        $invited->assertStatus(403)
        ->assertJsonFragment([
            'error' => 'User is already in assigned.'
        ]);
    }


    /** @test */
    function owner_cannot_add_not_company_employee () 
    {
        $this->withExceptionHandling();

        $project = $this->companyWithEmployeeAndProjectSignIn();

        $todo = factory(Todo::class)->create([
            'project_id' => $project->owner->id
        ]);

        $invited = $this->post($todo->path()."/invite", [
            'value' => true,
            'email' => 'notamember@email.com'
        ]);

        $invited->assertStatus(403)
        ->assertJsonFragment([
            'error' => 'Unable to process your request.'
        ]);
    }

    /** @test */
    function owner_cann_remove_employee () 
    {
        $this->withExceptionHandling();

        $project = $this->companyWithEmployeeAndProjectSignIn();

        $todo = factory(Todo::class)->create([
            'project_id' => $project->owner->id
        ]);

        $employees = $project->employees;

        $todo->employees()->attach($employees[0], ['identifier' => Str::uuid()]);
        
        $invited = $this->post($todo->path()."/invite", [
            'value' => false,
            'email' => $employees[0]->email
        ]);

        $invited->assertStatus(200)
        ->assertJsonFragment([
            'success' => 'User has been removed from Todo.'
        ]);
    }
}
