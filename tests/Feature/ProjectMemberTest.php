<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Model\Project;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectMemberTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function user_can_add_its_employee_to_project () 
    {
        $this->withExceptionHandling();
        $company = $this->companyWithEmployeeSignIn();
        $project = factory(Project::class)->create([
                'identifier'=> '040f4a2e-b57f5-478b-8e33-8c86c94e8633', 
                'owner_id' => $company->owner->id
            ]);
        
        $invited = $this->post($project->path()."/invite", [
            'value' => true,
            'email' => $company->owner->ownCompany->employees[0]->email
        ]);

        $invited->assertStatus(200);

        $this->assertTrue($project->employees->contains($company->owner->ownCompany->employees[0]));
    }


    /** @test */
    function owner_cannot_add_self_as_member_to_project () 
    {
        $this->withExceptionHandling();
        $company = $this->companyWithEmployeeSignIn();
        $project = factory(Project::class)->create([
                'identifier'=> '040f4a2e-b57f5-478b-8e33-8c86c94e8633', 
                'owner_id' => $company->owner->id
            ]);
        
        $invited = $this->post($project->path()."/invite", [
            'value' => true,
            'email' => $company->owner->email
        ]);

        $invited->assertStatus(403)
        ->assertJsonFragment([
            'error' => 'You own this project.'
        ]);
    }


    /** @test */
    function owner_cannot_same_employee_twice () 
    {
        $this->withExceptionHandling();
        $company = $this->companyWithEmployeeSignIn();
        $project = factory(Project::class)->create([
                'identifier'=> '040f4a2e-b57f5-478b-8e33-8c86c94e8633', 
                'owner_id' => $company->owner->id
            ]);
            
        $project->employees()->attach($company->owner->ownCompany->employees[0], ['identifier' => Str::uuid()]);
        
        $invited = $this->post($project->path()."/invite", [
            'value' => true,
            'email' => $company->owner->ownCompany->employees[0]->email
        ]);

        $invited->assertStatus(403)
        ->assertJsonFragment([
            'error' => 'User is already in Project.'
        ]);
    }


    /** @test */
    function owner_cannot_add_not_company_employee () 
    {
        $this->withExceptionHandling();
        $company = $this->companyWithEmployeeSignIn();
        $newUser = factory(User::class)->create();

        $project = factory(Project::class)->create([
                'identifier'=> '040f4a2e-b57f5-478b-8e33-8c86c94e8633', 
                'owner_id' => $company->owner->id
            ]);
        
        $invited = $this->post($project->path()."/invite", [
            'value' => true,
            'email' => $newUser->email
        ]);

        $invited->assertStatus(403)
        ->assertJsonFragment([
            'error' => 'Unable to process your request.'
        ]);
    }

    /** @test */
    function owner_cann_remove_employee () 
    {
        $this->withExceptionHandling();
        $company = $this->companyWithEmployeeSignIn();
        $project = factory(Project::class)->create([
                'identifier'=> '040f4a2e-b57f5-478b-8e33-8c86c94e8633', 
                'owner_id' => $company->owner->id
            ]);
            
        $project->employees()->attach($company->owner->ownCompany->employees[0], ['identifier' => Str::uuid()]);
        
        $detach = $this->post($project->path()."/invite", [
            'value' => false,
            'email' => $company->owner->ownCompany->employees[0]->email
        ]);

        $detach->assertStatus(200)
        ->assertJsonFragment([
            'success' => 'User has been removed from Project.'
        ]);
    }


}
