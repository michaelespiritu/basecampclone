<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Model\Project;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function a_user_can_create_project()
    {
        $this->signIn();

        $this->post('/project', $atts = [
            'name' => 'Name',
            'description' => 'Project Description'
        ])
        ->assertStatus(201)
        ->assertJsonFragment([
            'success' => 'Project has been Created.'
        ]);


        $this->assertDatabaseHas('projects', $atts);
    }


    /** @test */
    public function a_user_cant_create_project_without_name()
    {
        $this->signIn();

        $this->post('/project', $atts = [])
        ->assertSessionHasErrors('name')
        ->assertStatus(302);

    }


    /** @test */
    public function a_user_edit_the_project()
    {
        $this->withExceptionHandling();

        $project = factory(Project::class)->create(['identifier'=> '040f4a2e-b57f5-478b-8e33-8c86c94e8633']);
        $user = $this->signIn($project->owner);

        $this->patch($project->path(), $atts = [
            'name' => 'New Updated Name',
            'description' => 'New Updated Project Description'
        ])->assertStatus(200);

        
        $this->assertDatabaseHas('projects', $atts);
    }


    /** @test */
    public function a_user_can_delete_the_project()
    {
        $this->withExceptionHandling();

        $project = factory(Project::class)->create(['identifier'=> '040f4a2e-b57f5-478b-8e33-8c86c94e8633']);
        $user = $this->signIn($project->owner);

        $this->delete($project->path())
            ->assertStatus(200)
            ->assertJsonFragment([
                'success' => 'Project has been Deleted.'
            ]);

        
        $this->assertDatabaseMissing('projects', [
            'id' => $project->id
        ]);
    }
}
