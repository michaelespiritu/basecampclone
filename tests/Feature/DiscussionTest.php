<?php

namespace Tests\Feature;

use App\Model\Todo;
use Tests\TestCase;
use App\Model\Project;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DiscussionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_send_todo_discussion()
    {
        $this->withExceptionHandling();

        $todo = factory(Todo::class)->create();
        $user = $this->signIn($todo->project->owner);

        $this->post("/discussion/$todo->id/todo", [
            'body' => 'This is todo discussion body'
        ])->assertStatus(200);

        $this->assertDatabaseHas('discussions', [
            'body' => 'This is todo discussion body',
            'discussion_type' => 'todo',
            'discussion_id' => $todo->id
        ]);
    }


    /** @test */
    public function a_user_can_view_todo_discussion()
    {
        $this->withExceptionHandling();

        $todo = factory(Todo::class)->create();
        $user = $this->signIn($todo->project->owner);

        $this->post("/discussion/$todo->id/todo", [
            'body' => 'This is todo discussion body'
        ])->assertStatus(200);

        $this->assertDatabaseHas('discussions', [
            'body' => 'This is todo discussion body',
            'discussion_type' => 'todo',
            'discussion_id' => $todo->id
        ]);

        $this->get("/discussion/$todo->id/todo")
            ->assertJsonFragment([
                'body' => 'This is todo discussion body',
                'sender' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'profile' => $user->getProfile()
                ]
            ]);
    }


    /** @test */
    public function a_user_can_send_project_discussion()
    {
        $this->withExceptionHandling();

        $project = factory(Project::class)->create();
        $user = $this->signIn($project->owner);

        $this->post("/discussion/$project->id/project", [
            'body' => 'This is project discussion body'
        ])->assertStatus(200);

        $this->assertDatabaseHas('discussions', [
            'body' => 'This is project discussion body',
            'discussion_type' => 'project',
            'discussion_id' => $project->id
        ]);
    }


    /** @test */
    public function a_user_can_view_project_discussion()
    {
        $this->withExceptionHandling();

        $project = factory(Project::class)->create();
        $user = $this->signIn($project->owner);

        $this->post("/discussion/$project->id/project", [
            'body' => 'This is project discussion body'
        ])->assertStatus(200);

        $this->assertDatabaseHas('discussions', [
            'body' => 'This is project discussion body',
            'discussion_type' => 'project',
            'discussion_id' => $project->id
        ]);

        $this->get("/discussion/$project->id/project")
            ->assertJsonFragment([
                'body' => 'This is project discussion body',
                'sender' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'profile' => $user->getProfile()
                ]
            ]);
    }
}
