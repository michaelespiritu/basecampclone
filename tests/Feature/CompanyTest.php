<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Model\Company;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_can_create_one_company()
    {
        $this->withExceptionHandling();

        $user = $this->signIn();

        $company = $this->post('/company' , $attr = [
            'name' => 'New Name'
        ]);
        
        $company->assertStatus(200)
        ->assertJsonFragment([
            'success' => 'Company has been created.'
        ]);

        $this->assertDatabaseHas('companies', $attr);
    }


    /** @test */
    function a_user_cannot_create_more_than_one_company()
    {

        $this->withExceptionHandling();
        $company = factory(Company::class)->create();

        $this->signIn($company->owner);

        $company = $this->post('/company' , $attr = [
            'name' => 'New Name'
        ]);

        $company
        ->assertStatus(403)
        ->assertJsonFragment([
            'error' => 'You are not allowed to create more than 1 company.'
        ]);

    }


    /** @test */
    function a_user_cannot_add_company_if_no_name()
    {
        

        $this->signIn();

        $update = $this->post('/company' , [
            'name' => ''
        ]);

        // dd($update);
        // $update->assertSessionHasErrors('name')
        $update->assertStatus(302);

    }

     /** @test */
     function a_user_can_update_company()
     {
 
         $this->withExceptionHandling();
         $company = factory(Company::class)->create();
 
         $this->signIn($company->owner);
 
         $company = $this->patch($company->path() , $attr = [
             'name' => 'New Edited Name'
         ]);
 
         $company
         ->assertStatus(200)
         ->assertJsonFragment([
            'success' => 'Company has been Updated.'
         ]);
 

         $this->assertDatabaseHas('companies', $attr);
     }
}
