<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileTest extends TestCase
{

    use RefreshDatabase;


    /** @test */
    public function user_can_update_profile()
    {
        $user = $this->signIn();

        $this->post('/profile', [
                'timezone' => [
                    'Name' => '(UTC-08:00) Tijuana',
                    'Value' => 'America/Tijuana'
                ]
            ]
        );

        $this->assertDatabaseHas('profiles', [
            'timezone' => json_encode([
                'Name' => '(UTC-08:00) Tijuana',
                'Value' => 'America/Tijuana'
            ])
        ]);
    }


    /** @test */
    public function user_cant_use_duplicate_email()
    {
        $this->withExceptionHandling();
        $user1 = factory(User::class)->create();

        $user = $this->signIn();

        $email = $this->post('/profile/update-email', [
                'old_email' => $user->email,
                'new_email' => $user1->email
            ]
        );

        $email->assertStatus(403)
            ->assertJsonFragment([
                'error' => 'Email is already registered'
            ]);

    }

    /** @test */
    public function user_cant_proceed_if_old_email_is_incorrect()
    {
        $this->withExceptionHandling();

        $user = $this->signIn();

        $email = $this->post('/profile/update-email', [
                'old_email' => 'wrongold@email.com',
                'new_email' => 'newemail@email.com'
            ]
        );

        $email->assertStatus(403)
            ->assertJsonFragment([
                'error' => 'Old Email is incorrect'
            ]);

    }

    /** @test */
    public function user_can_update_email()
    {
        $this->withExceptionHandling();
        $user = $this->signIn();

        $email = $this->post('/profile/update-email', [
                'old_email' => $user->email,
                'new_email' => 'newemail@test.com'
            ]
        );

        $email->assertStatus(200)
            ->assertJsonFragment([
                'success' => 'Email has been updated'
            ]);

        $this->assertDatabaseHas('users', [
            'email' => 'newemail@test.com'
        ]);
    }


    /** @test */
    public function user_can_update_password()
    {
        $this->withExceptionHandling();
        $user = $this->signIn(factory(User::class)->create(['password' => Hash::make('oldpassword')]));

        $password = $this->post('/profile/update-password', [
                'old_password' => 'oldpassword',
                'new_password' => 'newpassword'
            ]
        );

        $password->assertStatus(200)
            ->assertJsonFragment([
                'success' => 'Password has been updated'
            ]);

        $this->assertTrue(Hash::check('newpassword', $user->password));
    }

    /** @test */
    public function user_cant_use_old_password()
    {
        $this->withExceptionHandling();
        $user = $this->signIn(factory(User::class)->create(['password' => Hash::make('oldpassword')]));

        $password = $this->post('/profile/update-password', $attr = [
                'old_password' => 'oldpassword',
                'new_password' => 'oldpassword'
            ]
        );

        $password->assertStatus(403)
            ->assertJsonFragment([
                'error' => 'You can\'t use your old Password'
            ]);
    }


    /** @test */
    public function user_cant_proceed_if_old_password_is_incorrect()
    {
        $this->withExceptionHandling();
        $user = $this->signIn(factory(User::class)->create(['password' => Hash::make('oldpassword')]));

        $password = $this->post('/profile/update-password', $attr = [
                'old_password' => 'wrongoldpassword',
                'new_password' => 'newpassword'
            ]
        );

        $password->assertStatus(403)
            ->assertJsonFragment([
                'error' => 'Old Password Incorrect'
            ]);
    }
}
