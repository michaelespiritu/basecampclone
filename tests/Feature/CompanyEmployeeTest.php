<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Model\Company;
use App\Model\Project;
use Illuminate\Support\Str;
use App\Http\Resources\CompanyResource;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyEmployeeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function company_can_have_an_employee ()
    {
        $this->withExceptionHandling();

        $company = factory(Company::class)->create();
        $newUser = factory(User::class)->create();
 
        $this->signIn($company->owner);

        $invited = $this->post("{$company->path()}/invite", [
            'email' => $newUser->email
        ]);

        $invited->assertStatus(200)
        ->assertJsonFragment([
            'success' => 'User has been added.'
        ]);

        $this->assertTrue($company->employees->contains($newUser));
    }

    /** @test */
    function company_cannot_add_company_owner ()
    {
        $this->withExceptionHandling();

        $company = factory(Company::class)->create();
 
        $this->signIn($company->owner);

        $invited = $this->post("{$company->path()}/invite", [
            'email' => $company->owner->email
        ]);

        $invited->assertStatus(403)
        ->assertJsonFragment([
            'error' => 'You own this company.'
        ]);
    }


    /** @test */
    function company_cannot_add_same_employee_twice ()
    {
        $this->withExceptionHandling();

        $company = factory(Company::class)->create();
        $newUser = factory(User::class)->create();
 
        $this->signIn($company->owner);
        $company->employees()->attach($newUser, ['identifier' => Str::uuid()]);

        $invited = $this->post("{$company->path()}/invite", [
            'email' => $newUser->email
        ]);

        $invited->assertStatus(403)
        ->assertJsonFragment([
            'error' => 'User is already in Company.'
        ]);

    }

    /** @test */
    function company_can_remove_an_employee ()
    {
        $this->withExceptionHandling();

        $company = factory(Company::class)->create();
        $newUser = factory(User::class)->create();
 
        $this->signIn($company->owner);

        $company->employees()->attach($newUser, ['identifier' => Str::uuid()]);

        $invited = $this->post("{$company->path()}/detach", [
            'where' => 'all',
            'email' => $newUser->email
        ]);


        $invited->assertStatus(200)
        ->assertJsonFragment([
            'success' => 'User has been removed from your company, project and todo.'
        ]);

        $this->assertTrue(!$company->employees->contains($newUser));
    }


    /** @test */
    function owner_cannot_remove_employee_from_all_project () 
    {
        $this->withExceptionHandling();

        $company = factory(Company::class)->create();
        $newUser = factory(User::class)->create();
 
        $this->signIn($company->owner);

        $project = factory(Project::class)->create([
                'identifier'=> '040f4a2e-b57f5-478b-8e33-8c86c94e8633', 
                'owner_id' => $company->owner->id
            ]);

        $project2 = factory(Project::class)->create([
                'identifier'=> '123E4a2e-b57f5-478b-1e42-8c86c94e8633', 
                'owner_id' => $company->owner->id
            ]);


        $project->employees()->attach($newUser, ['identifier' => Str::uuid()]);
        $project2->employees()->attach($newUser, ['identifier' => Str::uuid()]);


        $invited = $this->post("{$company->path()}/detach", [
            'where' => 'project',
            'email' => $newUser->email
        ]);

        $invited->assertStatus(200)
        ->assertJsonFragment([
            'success' => 'User has been removed from your Projects.'
        ]);

        $this->assertTrue(!$project->employees->contains($newUser));

    }
}
