<?php

namespace Tests\Feature;

use Carbon\Carbon;
use App\Model\Todo;
use Tests\TestCase;
use App\Model\Project;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TodoTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_add_todo()
    {
        $this->withExceptionHandling();

        $user = $this->signIn();

        $project = factory(Project::class)->create([
            'owner_id' => $user->id
        ]);

        $todo = $this->post($project->path() . '/todo', $atts = [
            'name' => 'Todo Name',
            'description' => 'Todo description',
            'due_date' => '2019-04-12 00:00:00',
            'tasks' => [
                [
                    'details' => 'Task Details',
                    'done' => null,
                    'done_by' => null
                ]
            ]
        ]);

        $todo->assertStatus(201);
        $todo->assertJsonFragment([
            'success' => 'To Do has been Created.' 
        ]);

        $this->assertDatabaseHas('todos', Arr::except($atts, 'tasks'));
        $this->assertDatabaseHas('tasks', [
            'task' => 'Task Details',
            'done' => null,
            'done_by' => null
        ]);
    }

    /** @test */
    public function a_user_cant_create_todo_without_name()
    {
        $user = $this->signIn();

        $project = factory(Project::class)->create([
            'owner_id' => $user->id
        ]);

        $todo = $this->post($project->path() . '/todo', $atts = [
            'due_date' => '2019-04-12 00:00:00'
        ])
        ->assertSessionHasErrors('name')
        ->assertStatus(302);
    }

    /** @test */
    public function a_user_cant_create_todo_without_due_date()
    {
        $user = $this->signIn();

        $project = factory(Project::class)->create([
            'owner_id' => $user->id
        ]);

        $status = $this->post($project->path() . '/todo', $atts = [
            'name' => 'Todo Name'
        ])
        ->assertSessionHasErrors('due_date')
        ->assertStatus(302);
    }

    /** @test */
    public function a_user_can_view_todo()
    {
        $this->withExceptionHandling();

        $project = $this->signIn(factory(Project::class)->create()->owner);

        $todo = factory(Todo::class)->create([
            'project_id' => $project->id
        ]);

        $update = $this->get($todo->path());
        $update->assertStatus(200);
        
        $update->assertSee($todo->name);

    }


    /** @test */
    public function a_user_can_view_edit_todo()
    {
        $this->withExceptionHandling();

        $project = $this->signIn(factory(Project::class)->create()->owner);

        $todo = factory(Todo::class)->create([
            'project_id' => $project->id
        ]);

        $update = $this->get($todo->path() . '/edit');

        $update->assertStatus(200);
        
        $update->assertSee($todo->name);

    }

    /** @test */
    public function a_user_can_update_todo()
    {
        $this->withExceptionHandling();

        $project = $this->signIn(factory(Project::class)->create()->owner);

        $todo = factory(Todo::class)->create([
            'project_id' => $project->id
        ]);

        $update = $this->patch($todo->path(), $atts = [
            'name' => 'New Name'
        ]);

        $update->assertStatus(200);
        
        $this->assertDatabaseHas('todos', $atts);

    }


    /** @test */
    public function a_user_can_update_todo_status()
    {
        $this->withExceptionHandling();

        $project = $this->signIn(factory(Project::class)->create()->owner);

        $todo = factory(Todo::class)->create([
            'project_id' => $project->id
        ]);

        $updateStatus = $this->post($todo->path() . '/update-status', $atts = [
            'status' => 'doing'
        ]);

        $updateStatus->assertStatus(200);
        
        $this->assertDatabaseHas('todos', [
            'status' => 'doing'
        ]);
    }


    /** @test */
    public function a_user_can_update_todo_due_date()
    {
        $this->withExceptionHandling();

        $project = $this->signIn(factory(Project::class)->create()->owner);

        $todo = factory(Todo::class)->create([
            'project_id' => $project->id
        ]);

        $updateStatus = $this->post($todo->path() . '/change-due-date', $atts = [
            'due_date' => '2019-09-12 00:00:00'
        ]);

        $updateStatus->assertStatus(200)
            ->assertJsonFragment([
                'success' => 'Due Date has been Updated.',
                'date' => Carbon::create('2019-09-12 00:00:00')->format('F j, Y')
            ]);
        
        $this->assertDatabaseHas('todos', [
            'due_date' => '2019-09-12 00:00:00'
        ]);
    }

    /** @test */
    public function a_user_cant_update_todo_due_date_without_date()
    {
        $this->withExceptionHandling();

        $project = $this->signIn(factory(Project::class)->create()->owner);

        $todo = factory(Todo::class)->create([
            'project_id' => $project->id
        ]);

        $updateStatus = $this->post($todo->path() . '/change-due-date', $atts = [
            'due_date' => ''
        ])
        ->assertSessionHasErrors('due_date')
        ->assertStatus(302);
    }


    /** @test */
    public function a_user_can_update_todo_order()
    {
        $this->withExceptionHandling();

        $project = $this->signIn(factory(Project::class)->create()->owner);

        $todo1 = factory(Todo::class)->create(
            [
                'project_id' => $project->id,
                'name' => 'Todo 1',
                'identifier' => 'f9a25524-9cbd-4be0-aaf8-886b7e051252',
                'order' => 1
            ]
        );

        $todo2 = factory(Todo::class)->create([
            'project_id' => $project->id,
            'name' => 'Todo 2',
            'identifier' => 'f9a25524-9cbd-4be0-aaf8-886b7e051253',
            'order' => 2
        ]);

        $todo = [
            [
                'identifier' => $todo2->identifier
            ],
            [
                'identifier' => $todo1->identifier
            ]
        ];

        $updateOrder = $this->post("/todo/$project->id/order", $atts = [
            'todo' => collect($todo)
        ]);


        $updateOrder->assertStatus(200);
        
        $this->assertDatabaseHas('todos', [
            'name' => 'Todo 2',
            'order' => 0
        ]);
    }


    /** @test */
    public function a_user_can_delete_todo()
    {
        $this->withExceptionHandling();

        $project = $this->signIn(factory(Project::class)->create()->owner);

        $todo = factory(Todo::class)->create([
            'project_id' => $project->id
        ]);

        $delete = $this->delete($todo->path());

        $delete->assertStatus(200);
        $delete->assertJsonFragment([
            'success' => 'Todo has been Deleted.'
        ]);
        
        $this->assertDatabaseMissing('todos', [
            'id' => $todo->id
        ]);

    }
}
