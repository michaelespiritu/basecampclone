<?php

namespace Tests\Unit;

use App\Model\Todo;
use Tests\TestCase;
use App\Model\Project;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TodoTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function todo_path()
    {
        $todo = factory(Todo::class)->create();
        $this->assertEquals('/todo/' . $todo->identifier, $todo->path());
    }


    /** @test */
    public function it_belongs_to_a_project()
    {
        $todo = factory(Todo::class)->create();
        $this->assertInstanceOf(Project::class, $todo->project);
    }
}
