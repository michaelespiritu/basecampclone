<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Model\Project;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function project_path()
    {
        $project = factory(Project::class)->create();
        $this->assertEquals('/project/' . $project->identifier, $project->path());
    }


    /** @test */
    public function it_belongs_to_an_owner()
    {
        $project = factory(Project::class)->create();
        $this->assertInstanceOf(User::class, $project->owner);
    }
}
