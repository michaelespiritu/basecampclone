<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Model\Company;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function a_user_has_projects()
    {
        $user = factory(User::class)->create();
        $this->assertInstanceOf(Collection::class, $user->project);
    }


    /** @test */
    public function a_user_has_company()
    {
        $user = factory(User::class)->create();
        factory(Company::class)->create(['owner_id' => $user->id]);

        $this->assertInstanceOf(Company::class, $user->ownCompany);
    }
}
