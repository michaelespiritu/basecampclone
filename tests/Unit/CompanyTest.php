<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\Model\Company;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function company_path()
    {
        $company = factory(Company::class)->create();
        $this->assertEquals('/company/' . $company->identifier, $company->path());
    }


    /** @test */
    public function it_belongs_to_an_owner()
    {
        $project = factory(Company::class)->create();
        $this->assertInstanceOf(User::class, $project->owner);
    }
}
