<?php

namespace Tests\Unit;

use App\Model\Task;
use App\Model\Todo;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function task_path()
    {
        $task = factory(Task::class)->create();
        $this->assertEquals('/task/' . $task->identifier, $task->path());
    }


    /** @test */
    public function it_belongs_to_an_todo()
    {
        $task = factory(Task::class)->create();
        $this->assertInstanceOf(Todo::class, $task->todo);
    }
}
