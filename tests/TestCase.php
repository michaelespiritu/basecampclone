<?php

namespace Tests;

use App\User;
use App\Model\Company;
use App\Model\Project;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function signIn($user = null)
    {
        $user = $user ?: factory('App\User')->create();
        $this->actingAs($user);
        return $user;
    }


    protected function companyWithEmployeeSignIn()
    {
        $company = factory(Company::class)->create();
        $newUser = factory(User::class)->create();
 
        $this->actingAs($company->owner);

        $this->post("{$company->path()}/invite", [
            'email' => $newUser->email
        ]);

        return $company;
    }


    protected function companyWithEmployeeAndProjectSignIn()
    {
        $company = factory(Company::class)->create();
        $project = factory(Project::class)->create(['owner_id' => $company->owner->id]);
        $this->actingAs($company->owner);

        $newUser = factory(User::class)->create();
        $newUser2 = factory(User::class)->create();

        $company->employees()->attach($newUser, ['identifier' => Str::uuid()]);
        $company->employees()->attach($newUser2, ['identifier' => Str::uuid()]);

        $project->employees()->attach($newUser, ['identifier' => Str::uuid()]);
        $project->employees()->attach($newUser2, ['identifier' => Str::uuid()]);

        return $project;

    }
}
