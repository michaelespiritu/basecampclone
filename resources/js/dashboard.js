require('./bootstrap');

import Store from './Vuex/Project'

Vue.component('dashboard', require('./components/Dashboard.vue').default);

const app = new Vue({
    el: '#app',
    store: Store
});
