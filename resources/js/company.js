require('./bootstrap')

import Store from './Vuex/Company'

Vue.component('company', require('./components/Company/CompanyContainer.vue').default)

const app = new Vue({
    el: '#app',
    store: Store
})
