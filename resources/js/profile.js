require('./bootstrap')

import Store from './Vuex/Profile'

Vue.component('profile', require('./components/Profile/ProfileContainer.vue').default)

const app = new Vue({
    el: '#app',
    store: Store
})
