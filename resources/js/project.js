require('./bootstrap');

import Store from './Vuex/Project'

Vue.component('discussion', require('./components/Misc/Discussion.vue').default);
Vue.component('members', require('./components/Project/Members.vue').default);

const app = new Vue({
    el: '#app',
    store: Store
});
