require('./bootstrap');

import Store from './Vuex/Todo'

Vue.component('todo', require('./components/Todo/All/Container.vue').default);
Vue.component('edit', require('./components/Project/Edit.vue').default);
Vue.component('discussion', require('./components/Misc/Discussion.vue').default);
Vue.component('members', require('./components/Project/Members.vue').default);

const app = new Vue({
    el: '#app',
    store: Store
});
