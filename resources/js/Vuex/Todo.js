import Vue from 'vue';
import Vuex from 'vuex';

import ToDo from './Project/Todo';

Vue.use(Vuex);
Vue.config.debug = true;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        ToDo
    },
    strict: false
});
