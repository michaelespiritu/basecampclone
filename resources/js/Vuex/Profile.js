import Vue from 'vue';
import Vuex from 'vuex';

import Profile from './Web/Profile';

Vue.use(Vuex);
Vue.config.debug = true;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        Profile
    },
    strict: false
});
