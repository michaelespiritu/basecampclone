import Vue from 'vue';
import Vuex from 'vuex';

import Task from './Project/Task';

Vue.use(Vuex);
Vue.config.debug = true;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        Task
    },
    strict: false
});
