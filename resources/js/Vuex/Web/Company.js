const getDefaultState = () => {
    return {
        Company: [],
    }
  }
  
  const state = getDefaultState()
  
  
  const mutations = {
        SET_COMPANY(state, data) {
            state.Company = data;
        },
        RESET_COMPANY(state, data) {
            state.Company = [];
        }
  }
  
  
  export default {
      state,
      mutations
  }
  