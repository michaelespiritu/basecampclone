const getDefaultState = () => {
    return {
      User: [],
    }
  }
  
  const state = getDefaultState()
  
  
  const mutations = {
        SET_USER(state, data) {
            state.User = data;
        }
  }
  
  
  export default {
      state,
      mutations
  }
  