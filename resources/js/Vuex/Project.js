import Vue from 'vue';
import Vuex from 'vuex';

import Project from './Project/Project';

Vue.use(Vuex);
Vue.config.debug = true;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        Project
    },
    strict: false
});
