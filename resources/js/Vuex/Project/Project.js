const getDefaultState = () => {
    return {
      Projects: [],
      Employees: [],
      ShowCreateForm: false,
      LoadProject: true,
      Path: ''
    }
  }
  
  const state = getDefaultState()
  
  
  const actions = {
      get_project({ commit }, data) {
        commit('GET_PROJECT', data)
      },
      reset_project({ commit }) {
        commit('RESET_PROJECT')
      }
  }
  
  const mutations = {
        GET_EMPLOYEES(state, data) {
            state.Employees = data;
        },
        RESET_EMPLOYEES(state) {
            state.Employees = [];
        },
        GET_PROJECT(state, data) {
            state.Projects = data;
        },
        SHOW_PROJECT_CREATE_FORM(state, data) {
            state.ShowCreateForm = data;
        },
        LOAD_PROJECT(state, data) {
            state.LoadProject = data
        },
        SET_PATH(state, data) {
            state.Path = data
        },
        RESET_PROJECT(state) {
            state.Projects = []
        }
  }
  
  
  export default {
      state,
      mutations,
      actions
  }
  