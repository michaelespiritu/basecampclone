const getDefaultState = () => {
    return {
      ProjectId: 0,
      ProjectIdentifier: 0,
      ToDos: [],
      Doing: [],
      Done: [],
      TempData: [],
      ShowCreateForm: false,
      LoadToDos: true,
      SetOrder: false,
      Employees: [],
    }
  }
  
  const state = getDefaultState()
  
  
  const actions = {
      get_project({ commit }, data) {
        commit('GET_PROJECT', data)
      },
      reset_project({ commit }) {
        commit('RESET_PROJECT')
      }
  }
  
  const mutations = {
        GET_EMPLOYEES(state, data) {
            state.Employees = data;
        },
        RESET_EMPLOYEES(state) {
            state.Employees = [];
        },
        SAVE_PROJECT_ID(state, data) {
            state.ProjectId = data;
        },
        SAVE_PROJECT_IDENTIFIER(state, data) {
            state.ProjectIdentifier = data;
        },
        GET_TODOS(state, data) {
            state.ToDos = data;
        },
        UPDATE_TODO(state, data) {
            state.ToDos = data;
        },
        GET_DOING(state, data) {
            state.Doing = data;
        },
        UPDATE_DOING(state, data) {
            state.Doing = data;
        },
        GET_DONE(state, data) {
            state.Done = data;
        },
        UPDATE_DONE(state, data) {
            state.Done = data;
        },
        SET_ORDER(state, data) {
            state.SetOrder = data;
        },
        SET_TEMP_DATA(state, data) {
            state.TempData = data;
        },
        SHOW_TODO_CREATE_FORM(state, data) {
            state.ShowCreateForm = data;
        },
        LOAD_TODOS(state, data) {
            state.LoadToDos = data
        },
        RESET_ALL(state) {
            state.ToDos = []
            state.Doing = []
            state.Done = []
        }
  }
  
  
  export default {
      state,
      mutations,
      actions
  }
  