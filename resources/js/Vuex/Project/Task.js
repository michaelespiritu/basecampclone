const getDefaultState = () => {
    return {
      Data: [],
      ToDoIdentifier: '',
      ToDoId: '',
      User: [],
      Employees: [],
    }
  }
  
  const state = getDefaultState()
  
  
  const mutations = {
        GET_EMPLOYEES(state, data) {
            state.Employees = data;
        },
        RESET_EMPLOYEES(state) {
            state.Employees = [];
        },
        SET_TODO_IDENTIFIER(state, data) {
            state.ToDoIdentifier = data;
        },
        SET_TODO_ID(state, data) {
            state.ToDoId = data;
        },
        SET_USER(state, data) {
            state.User = data;
        },
        GET_TASK(state, data) {
            state.Data = data;
        },
        RESET_TASK(state) {
            state.Data = []
        }
  }
  
  
  export default {
      state,
      mutations
  }
  