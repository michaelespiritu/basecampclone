require('./bootstrap');

import Store from './Vuex/Project'

Vue.component('todo', require('./components/Todo/All/Container.vue').default);

const app = new Vue({
    el: '#app',
    store: Store
});
