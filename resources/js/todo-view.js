require('./bootstrap')

import Store from './Vuex/TodoView'

Vue.component('Status', require('./components/Todo/Status/Status.vue').default)
Vue.component('Task', require('./components/Todo/Task/Task.vue').default)
Vue.component('Edit', require('./components/Todo/Edit.vue').default)
Vue.component('DueDate', require('./components/Todo/Task/DueDate.vue').default)
Vue.component('Upload', require('./components/Misc/Upload.vue').default)
Vue.component('Discussion', require('./components/Misc/Discussion.vue').default)
Vue.component('AssignedTo', require('./components/Todo/Task/AssignedTo.vue').default)

const app = new Vue({
    el: '#app',
    store: Store
})
