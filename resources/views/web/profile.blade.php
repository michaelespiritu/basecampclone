@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-sm-12">

            @if (session('status'))
            <div class="card">
                <div class="card-header">Warning</div>

                <div class="card-body">
                    
                    <div class="alert alert-warning" role="alert">
                        {{ session('status') }}
                    </div>

                </div>
            </div>
            @endif
            <profile
                    :user="{{ json_encode($profile) }}"
                ></profile>

            

        </div>
    </div>
</div>
@endsection


@section('script')

<!-- Scripts -->
<script src="{{ asset('js/profile.js') }}"></script>

@endsection
