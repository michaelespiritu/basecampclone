@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-sm-12 px-1 py-1">

            <div class="row align-items-center mb-3">

                <div class="col-12 col-lg-9 mb-2 mb-lg-0 text-center text-lg-left">
                    <p class="text-capitalize breadcrumbs mb-0">
                        <a href="/dashboard">
                            <span class="mr-1">
                                All Project
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <a href="/project/{{$todo->project->identifier}}">
                            <span class="mr-1">
                                {{$todo->project->name}} 
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <a href="/project/{{$todo->project->identifier}}/todo">
                            <span class="mr-1">
                                Todo
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <a href="/todo/{{$todo->identifier}}">
                            <span class="mr-1">
                                {{$todo->name}} 
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <span class="mr-1">Edit</span>
                    </p>
                </div>


                <div class="col-12 col-lg-3 mb-2 mb-lg-0">
                    <a 
                        href="/todo/{{$todo->identifier}}/view"
                        class="btn btn-sm btn-primary w-100 text-white" >Cancel</a>
                </div>

            </div>


            <edit
                    :todo="{{$todo}}"
                ></edit>

        </div>
    </div>
</div>
@endsection


@section('script')

<!-- Scripts -->
<script src="{{ asset('js/todo-view.js') }}"></script>

@endsection
