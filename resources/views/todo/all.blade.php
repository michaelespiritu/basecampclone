@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-sm-12 px-1 py-1">

            <div class="row align-items-center mb-3">

                <div class="col-12 col-lg-9 mb-2 mb-lg-0 text-center text-lg-left">
                    <p class="text-capitalize breadcrumbs">
                        <a href="/dashboard">
                            <span class="mr-1">
                                All Project
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <a href="/project/{{$project->identifier}}">
                            <span class="mr-1">
                                {{$project->name}} 
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <span class="mr-1">Todo</span>
                    </p>
                </div>



            </div>


            <todo
                id="{{$project->id}}"
                identifier="{{$project->identifier}}"
                :todo="{{json_encode($todo)}}"
                :doing="{{json_encode($doing)}}"
                :done="{{json_encode($done)}}"
                :count="{{$count}}"
                :employees="{{json_encode($employees)}}"
                >
                </todo>

        </div>
    </div>
</div>
@endsection


@section('script')

<script src="{{ asset('js/todo.js') }}"></script>

@endsection
