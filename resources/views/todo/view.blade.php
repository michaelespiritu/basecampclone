@extends('layouts.app')

@section('style')


@endsection

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-sm-12 px-1 py-1">

            <div class="row align-items-center mb-3">

                <div class="col-12 col-lg-9 mb-2 mb-lg-0 text-center text-lg-left">
                    <p class="text-capitalize breadcrumbs">
                        <a href="/dashboard">
                            <span class="mr-1">
                                All Project
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <a href="/project/{{$todo->project->identifier}}">
                            <span class="mr-1">
                                {{$todo->project->name}} 
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <a href="/project/{{$todo->project->identifier}}/todo">
                            <span class="mr-1">
                                Todo
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <span class="mr-1">{{$todo->name}}</span>
                    </p>
                </div>


                <div class="col-12 col-lg-3 mb-2 mb-lg-0">
                    <a 
                        href="/todo/{{$todo->identifier}}/edit"
                        class="btn btn-sm btn-primary w-100 text-white" >Edit</a>
                </div>

            </div>

            <div class="card mb-3">
                <div class="card-body">
                    <div class="row align-items-center ">

                        <div class="col-12 col-md-3 mb-2 mb-md-0">
                            <status 
                                set-status="{{$todo->status}}"
                                name="{{$todo->name}}"></status>
                        </div>

                        <div class="col-12 col-md-9 order-md-first">
                            <h1 class="mb-0 text-capitalize">{{$todo->name}}</h1>
                            
                            <due-date 
                                date-created="{{$todo->created_at->format('F j, Y')}}"
                                due-date="{{$todo->due_date->format('Y-m-j')}}"
                                due-date-display="{{$todo->due_date->format('F j, Y')}}"></due-date>

                        </div>

                        <div class="col-12">
                            <assigned-to
                             :employees="{{json_encode($employees)}}"
                             ></assigned-to>
                        </div>

                    </div>


                    @if (!empty($todo->description))
                    <div class="my-3">
                        {!!$todo->description!!}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">

                <div class="col-12 mb-3">
                    <task 
                        :user="{{auth()->user()}}"
                        todo-identifier="{{$todo->identifier}}"
                        todo-id="{{$todo->id}}"
                        :tasks="{{$todo->task}}"></task>
                </div>

                <div class="col-12 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="text-capitalize">Brainstorming</h5>
                            <div class="dropdown-divider my-3"></div>
                                <discussion
                                    type="todo"
                                    identifier="{{$todo->id}}"
                                    ></discussion>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="text-capitalize">Media</h5>
                            <div class="dropdown-divider my-3"></div>
                            <upload></upload>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="text-capitalize">Members</h5>
                            <div class="dropdown-divider my-3"></div>
                        </div>
                    </div>
                </div>

            </div>
            
            



        </div>
    </div>
</div>
@endsection


@section('script')

<!-- Scripts -->
<script src="{{ asset('js/todo-view.js') }}"></script>

@endsection
