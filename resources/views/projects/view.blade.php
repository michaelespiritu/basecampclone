@extends('layouts.app')


@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-sm-12 py-1 mb-3">
            
            <div class="row align-items-center mb-3">

                <div class="col-12 col-lg-9 mb-2 mb-lg-0 text-center text-lg-left">
                    <p class="text-capitalize breadcrumbs mb-0">
                        <a href="/dashboard">
                            <span class="mr-1">
                                All Project
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <span class="mr-1">{{$project->name}} </span>
                    </p>
                </div>


                <div class="col-12 col-lg-3 mb-2 mb-lg-0">
                    <a 
                        href="/project/{{$project->identifier}}/edit"
                        class="btn btn-sm btn-primary w-100 text-white" >Edit</a>
                </div>

            </div>


            <div class="card">
                <div class="card-body">

                    <div class="text-left mb-3">
                        <h1 class="mb-0 text-capitalize">{{$project->name}}</h1>
                        <p class="mb-0">
                            <small>
                                Created at: {{$project->created_at->format('F j, Y')}}
                            </small>
                        </p>
                    </div>


                    @if (!empty($project->description))
                    <div class="my-1">
                        {!!$project->description!!}
                    </div>
                    @endif  
                </div>
            </div>
        </div>

        
        <div class="col-12 col-md-6 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="row justify-content-center align-items-center">
                        <div class="col-sm-12 col-md-6">
                            <h5 class="text-capitalize mb-0">Todo</h5>
                        </div>
                        <div class="col-sm-12 col-md-6 text-center  text-md-right">
                            <a class="btn btn-primary" href="/project/{{$project->identifier}}/todo">View All</a>
                        </div>
                    </div>
                    <div class="dropdown-divider my-3"></div>
                    
                    <button class="btn btn-primary btn-lg">
                        To Do <span class="badge badge-light">{{$project->todoCount()}}</span>
                    </button>
                    <button class="btn btn-secondary btn-lg">
                        Doing <span class="badge badge-light">{{$project->todoDoingCount()}}</span>
                    </button>
                    <button class="btn btn-success btn-lg">
                        Done <span class="badge badge-light">{{$project->todoDoneCount()}}</span>
                    </button>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-3">
            <div class="card">
                @if($project->owner->ownCompany)
                <members 
                    :members="{{json_encode($members)}}"
                    identifier="{{$project->identifier}}"
                    :company="{{json_encode($company)}}"
                    ></members>
                @else
                <div class="card-body">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-12 col-md-6">
                                <h5 class="text-capitalize mb-0">Members</h5>
                            </div>
                            <div class="col-sm-12 col-md-6 text-center  text-md-right">
                                <a class="btn btn-primary" href="/company">Create Company</a>
                            </div>
                        </div>

                        <div class="dropdown-divider my-3"></div>

                        <p class="text-center mb-0">Create your Company first and add Employee to unlock this feature.</p>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-12 mb-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="text-capitalize">Discussion</h5>
                    <div class="dropdown-divider my-3"></div>
                    <discussion
                        type="project"
                        identifier="{{$project->id}}"
                        ></discussion>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection


@section('script')

<!-- Scripts -->
<script src="{{ asset('js/project.js') }}"></script>

@endsection
