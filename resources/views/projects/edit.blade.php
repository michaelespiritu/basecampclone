@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-sm-12 py-1 mb-0">
            <div class="row align-items-center mb-3">

                <div class="col-12 col-lg-9 mb-2 mb-lg-0 text-center text-lg-left">
                    <p class="text-capitalize breadcrumbs mb-0">
                        <a href="/dashboard">
                            <span class="mr-1">
                                All Project
                            </span>
                        </a>
                        <span class="mr-1">&rsaquo; </span>
                        <span class="mr-1"><a href="/project/{{$project->identifier}}">{{$project->name}}</a></span>
                        <span class="mr-1">&rsaquo; </span>
                        <span class="mr-1">Edit</span>

                    </p>
                </div>


                <div class="col-12 col-lg-3 mb-2 mb-lg-0">
                    <a 
                        href="/project/{{$project->identifier}}"
                        class="btn btn-sm btn-primary w-100 text-white" >Cancel</a>
                </div>

            </div>
        
        </div>

        <edit
            :project="{{$project}}"
            >
            </edit>




    </div>
</div>
@endsection


@section('script')

<script src="{{ asset('js/required.js') }}"></script>
<script src="{{ asset('js/todo.js') }}"></script>

@endsection
