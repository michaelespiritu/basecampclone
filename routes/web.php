<?php


Route::get('/', function () {
     return redirect('/login');
});

Auth::routes(['verify' => true]);
    
Route::group(['middleware' => 'auth'], function () {
    Route::get('profile', 'Web\ProfieController@index')->name('profile.index');
    Route::post('profile', 'Web\ProfieController@store')->name('profile.store');
    Route::post('profile/update-email', 'Web\ProfieController@updateEmail')->name('profile.update.email');
    Route::post('profile/update-password', 'Web\ProfieController@updatePassword')->name('profile.update.password');
});

Route::group(['middleware' => ['verified', 'auth']], function () {
    Route::get('/dashboard', 'HomeController@index')->name('home');

    Route::resource('project', 'Project\ProjectController');
    Route::post('/project/{project}/invite', 'Project\ProjectController@invite')->name('project.invite');

    Route::get('/project/{project}/todo', 'Project\TodoController@index')->name('todo.index');
    Route::post('/project/{project}/todo', 'Project\TodoController@store')->name('todo.store'); 

    Route::resource('todo', 'Project\TodoController')->except([
        'index', 'create'
    ]);

    Route::post('/todo/{todo}/invite', 'Project\TodoController@invite')->name('project.invite');
    Route::post('/todo/{todo}/update-status', 'Project\TodoController@updateStatus')->name('todo.update.status');
    Route::post('/todo/{todo}/change-due-date', 'Project\TodoController@changeDueDate')->name('todo.change.date');
    Route::post('/todo/{project}/order', 'Project\TodoController@updateOrder')->name('todo.update.order');

    Route::post('/task/{task}/update/done', 'Project\TaskController@updateDone')->name('task.update.done');
    Route::post('/todo/{todo}/task', 'Project\TaskController@store')->name('task.store');

    Route::get('/discussion/{id}/{type}', 'Project\DiscussionController@getDiscussion')->name('discussion.get');
    Route::post('/discussion/{id}/{type}', 'Project\DiscussionController@sendDiscussion')->name('discussion.send');

    Route::apiResource('company', 'Web\CompanyController')->except([
        'show', 'destroy'
    ]);
    Route::post('/company/{company}/invite', 'Web\CompanyController@invite')->name('company.invite');
    Route::post('/company/{company}/detach', 'Web\CompanyController@detach')->name('company.detach');

});
