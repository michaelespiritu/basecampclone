<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model\Todo;
use App\Model\Project;
use Faker\Generator as Faker;

$factory->define(Todo::class, function (Faker $faker) {

    return [
        'project_id' => factory(Project::class)->create(),
        'identifier' => Str::uuid(),
        'name' => ucfirst($faker->text),
        'description' => $faker->text,
        'due_date' => '2019-08-31',
        'status' => 'todo',
        'order' => 0
    ];
});
