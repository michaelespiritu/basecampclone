<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model\Task;
use App\Model\Todo;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'todo_id' => factory(Todo::class)->create(),
        'identifier' => Str::uuid(),
        'task' => ucfirst($faker->text),
        'done' => null,
        'done_by' => null
    ];
});
