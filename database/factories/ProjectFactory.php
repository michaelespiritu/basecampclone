<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use App\Model\Project;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'owner_id' => factory(User::class)->create(),
        'identifier' => Str::uuid(),
        'member_id' => [1],
        'name' => ucfirst($faker->text),
        'description' => $faker->text
    ];
});
