<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use App\Model\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'owner_id' => factory(User::class)->create(),
        'identifier' => Str::uuid(),
        'name' => ucfirst($faker->company)
    ];
});
