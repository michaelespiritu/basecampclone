const mix = require('laravel-mix');


mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/project.js', 'public/js/project.js')
    .js('resources/js/dashboard.js', 'public/js/dashboard.js')
    .js('resources/js/profile.js', 'public/js/profile.js')
    .js('resources/js/company.js', 'public/js/company.js')
    .js('resources/js/todo.js', 'public/js/todo.js')
    .js('resources/js/todo-view.js', 'public/js/todo-view.js')
    .sass('resources/sass/app.scss', 'public/css')
    .webpackConfig({
        resolve: {
            alias: {
                '@': path.resolve('resources/assets/sass')
            }
        }
    })