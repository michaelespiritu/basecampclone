<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    // public function employee()
    // {
    //     return $this->belongsToMany(User::class, 'company_employee')->withTimestamps();
    // }


    public function employees()
    {
        return $this->morphToMany(User::class, 'taggables')->withTimestamps();
    }


    public function path()
    {
        return "/company/$this->identifier";
    }
}
