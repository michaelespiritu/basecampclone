<?php

namespace App\Model;

use App\Model\Todo;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];

    protected $dates = [
        'done',
    ];

    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function todo ()
    {
        return $this->belongsTo(Todo::class, 'todo_id');
    }

    public function path()
    {
        return "/task/$this->identifier";
    }
}
