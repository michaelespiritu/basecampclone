<?php

namespace App\Model;

use App\User;
use App\Model\Task;
use App\Model\Project;
use App\Model\Discussion;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\DiscussionResource;

class Todo extends Model
{
    protected $casts = [
        'member_id' => 'json'
    ];

    protected $dates = [
        'due_date',
    ];

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function project ()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function task ()
    {
        return $this->hasMany(Task::class, 'todo_id');
    }

    /**
     * Get the todo's discussion.
     */
    public function discussions()
    {
        return $this->morphMany(Discussion::class, 'discussion');
    }

    public function employees()
    {
        return $this->morphToMany(User::class, 'taggables')->withTimestamps();
    }



    public function path()
    {
        return "/todo/$this->identifier";
    }

}
