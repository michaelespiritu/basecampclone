<?php

namespace App\Model;

use App\User;
use Laravolt\Avatar\Facade as Avatar;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $casts = [
        'timezone' => 'json'
    ];

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function getProfileImageAttribute($value)
    {
        return ($value) 
            ? "<img src='". $value . "' alt='" . $this->user->name . "'>" 
            : "<img src='".Avatar::create($this->user->name)
                ->setBackground($this->background)
                ->setBorder(1, $this->background)
                ->setShape('square')
                ->toBase64()."' 
                alt='" . $this->user->name . "' 
                class='rounded-circle' 
                style='border: 1px solid $this->background; background: $this->background'>";
    }

}
