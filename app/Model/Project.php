<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use App\Model\Todo;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    
    protected $casts = [
        'member_id' => 'json'
    ];

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function owner ()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }


    public function todo ()
    {
        return $this->hasMany(Todo::class, 'project_id');
    }

    /**
     * Get the todo's discussion.
     */
    public function discussions()
    {
        return $this->morphMany(Discussion::class, 'discussion');
    }

    public function employees()
    {
        return $this->morphToMany(User::class, 'taggables')->withTimestamps();
    }

    
    public function todoCount()
    {
        return count($this->todo()->where('status', 'todo')->get());
    }


    public function todoDoingCount()
    {
        return count($this->todo()->where('status', 'doing')->get());
    }

    
    public function todoDoneCount()
    {
        return count($this->todo()->where('status', 'done')->get());
    }

    public function path()
    {
        return "/project/$this->identifier";
    }

}
