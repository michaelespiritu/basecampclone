<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }


    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }

    /**
     * Get the owning imageable model.
     */
    public function discussion()
    {
        return $this->morphTo();
    }
}
