<?php

namespace App;

use App\Model\Company;
use App\Model\Profile;
use App\Model\Project;
use Laravolt\Avatar\Facade as Avatar;
use App\Http\Resources\ProfileResource;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }


    public function getProfile()
    {
        return ($this->profile) 
                ? ProfileResource::make($this->profile) 
                : ['profile_image' => "<img src='".Avatar::create($this->name)
                                ->setBackground(config('app.background'))
                                ->setBorder(1, config('app.background'))
                                ->setFont(public_path().'/fonts/Raleway.ttf')
                                ->setShape('square')
                                ->toBase64()."
                                'alt='" . $this->name . "'
                                class='rounded-circle' 
                                style='border: 1px solid ".config('app.background')."; background: ".config('app.background').";'>"];
    }


    public function project()
    {
        return $this->hasMany(Project::class, 'owner_id');
    }


    public function ownCompany()
    {
        return $this->hasOne(Company::class, 'owner_id');
    }


    public function company()
    {
        return $this->morphedByMany(Company::class, 'taggables');
    }

}
