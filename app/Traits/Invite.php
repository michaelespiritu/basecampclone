<?php

namespace App\Traits; 

use App\User;
use Exception;
use Illuminate\Support\Str;

trait Invite {


    public function inviteUser($model, $request, $type)
    {
        $user = User::whereEmail($request['email'])->first();

        if ($type == 'company') {
            if ($this->validateInviteCompany($user, $model) != false) {
                return $this->validateInviteCompany($user, $model);
            } 

            $model->employees()->attach($user, ['identifier' => Str::uuid()]);
            
            return [
                'status' => 200,
                'type' => 'success',
                'message' => 'User has been added.'
            ];

        }

        if ($type == 'project') {
            if ($this->validateInviteProject($user, $model) != false) {
                return $this->validateInviteProject($user, $model);
            } 

            $model->employees()->attach($user, ['identifier' => Str::uuid()]);

            return [
                'status' => 200,
                'type' => 'success',
                'message' => 'User has been added.'
            ];
        }


        if ($type == 'todo') {
            if ($this->validateInviteTodo($user, $model) != false) {
                return $this->validateInviteTodo($user, $model);
            } 

            $model->employees()->attach($user, ['identifier' => Str::uuid()]);

            return [
                'status' => 200,
                'type' => 'success',
                'message' => 'User has been added.'
            ];
        }

        return false;
    }

    private function validateInviteCompany($user, $company)
    {
        if ($company->owner->id == $user->id) {
            return [
                'status' => 403,
                'type' => 'error',
                'message' => 'You own this company.'
            ];
        }

        if ($company->employees->contains($user)) {
            return [
                'status' => 403,
                'type' => 'error',
                'message' => 'User is already in Company.'
            ];
        }

        return false;
    }


    public function validateInviteProject($user, $project)
    {
        if ($project->owner->id == $user->id) {
            return [
                'status' => 403,
                'type' => 'error',
                'message' => 'You own this project.'
            ];
        }

        if ($project->employees->contains($user)) {
            return [
                'status' => 403,
                'type' => 'error',
                'message' => 'User is already in Project.'
            ];
        }

        if (!$project->owner->ownCompany->employees->contains($user)) {
            return [
                'status' => 403,
                'type' => 'error',
                'message' => 'Unable to process your request.'
            ];
        }

        return false;
    }


    public function validateInviteTodo($user, $todo)
    {
  
        if ($todo->employees->contains($user)) {
            return [
                'status' => 403,
                'type' => 'error',
                'message' => 'User is already in assigned.'
            ];
        }

        if (!$todo->project->owner->ownCompany->employees->contains($user)) {
            return [
                'status' => 403,
                'type' => 'error',
                'message' => 'Unable to process your request.'
            ];
        }

        return false;
    }


    public function detachUser($model, $request, $data)
    {
        
       
    }
}