<?php

namespace App\Http\Controllers\Project;

use App\Model\Discussion;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\DiscussionResource;

class DiscussionController extends Controller
{
    

    public function getDiscussion($id, $type)
    {
        $discussion = Discussion::where('discussion_id', $id)->where('discussion_type', $type)->get();
        return DiscussionResource::collection($discussion);
    }


    public function sendDiscussion($id, $type)
    {
        Discussion::create([
            'identifier' => Str::uuid(),
            'body' => request()->body,
            'discussion_id' => $id,
            'discussion_type' => $type,
            'sender_id' => auth()->user()->id,
        ]);
        
        return DiscussionResource::collection(Discussion::where('discussion_id', $id)->where('discussion_type', $type)->get());
    }
}
