<?php

namespace App\Http\Controllers\Project;

use App\User;
use Carbon\Carbon;
use App\Model\Todo;
use App\Model\Project;
use App\Traits\Invite;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Resources\TodoResource;
use App\Http\Resources\UserResource;

class TodoController extends Controller
{

    use Invite;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        if (request()->v == 'j') {
            return [
                'todo' => TodoResource::collection($project->todo()->where('status', 'todo')->orderBy('order', 'asc')->get()),
                'doing' => TodoResource::collection($project->todo()->where('status', 'doing')->orderBy('order', 'asc')->get()),
                'done' => TodoResource::collection($project->todo()->where('status', 'done')->orderBy('order', 'asc')->get()),
                'count' => count($project->todo()->get()),
                'employees' => UserResource::collection($project->employees)
            ];
        }

        return view('todo.all')
            ->with('project', $project)
            ->with('todo', TodoResource::collection($project->todo()->where('status', 'todo')->orderBy('order', 'asc')->get()))
            ->with('doing', TodoResource::collection($project->todo()->where('status', 'doing')->orderBy('order', 'asc')->get()))
            ->with('done', TodoResource::collection($project->todo()->where('status', 'done')->orderBy('order', 'asc')->get()))
            ->with('count', count($project->todo()->get()))
            ->with('employees', UserResource::collection($project->employees));
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project)
    {
        request()->validate(['name' => 'required', 'due_date' => 'required']);

        $count = count($project->todo()->get()) + 1;

        $todo = $project->todo()->create(
            [   
                'identifier' => Str::uuid(),
                'name' => ucfirst(request()->name),
                'description' => request()->description,
                'due_date' => request()->due_date,
                'status' => 'todo',
                'order' => $count
            ]
        );

        
        if (!empty(request()->tasks[0]['details'])) {
            $this->createTask(request()->tasks, $todo);
        }

        if (!empty(request()->assignTo)) {
            foreach(request()->assignTo as $assign) {
                $this->attach($todo, $assign);
            }
        }
        
        return response()->json([
            'success' => 'To Do has been Created.'
        ], 201);
    }


    private function createTask($tasks, $todo)
    {
        foreach ($tasks as $task) {
            $todo->task()->create([
                'identifier' => Str::uuid(),
                'task' => ucfirst($task['details']),
                'done' => null,
                'done_by' => null
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        return view('todo.view')
            ->with('todo', TodoResource::make($todo))
            ->with('employees', UserResource::collection($todo->employees));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        $this->authorize('update', $todo->project);
        return view('todo.edit')->with('todo', $todo);
    }

    /**
     * Update the specified status of todo in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Todo $todo)
    {
        
        $todo->update([
            'status' => request()->status
        ]);

        return response()->json([
            'success' => "$todo->name has been marked as " . request()->status
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Todo $todo)
    {
        $this->authorize('update', $todo->project);

        request()->validate(['name' => 'required', 'description' => 'sometimes|required', 'due_date' => 'sometimes|required']);

        $todo->update([
            'name' => ucfirst(request()->name),
            'description' => (!empty(request()->description)) ? request()->description : $todo->description,
            'due_date' => (!empty(request()->due_date)) ? request()->due_date : $todo->due_date
        ]);

        return response()->json([
            'success' => 'To Do has been Updated.'
        ], 200);
    }

    /**
     * Update the order of todo in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOrder($id)
    {
        $order = [];

        foreach(request()->todo as $key=>$todo) {
            $insert = Todo::where('identifier', $todo['identifier'])->where('project_id', $id)->update([
                'order' => $key
            ]);
            array_push($order, $insert);
        }

        return $order;
    }


    /**
     * Update the due date specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeDueDate(Todo $todo)
    {
        request()->validate(['due_date' => 'required']);

        $todo->update([
            'due_date' => request()->due_date
        ]);

        return response()->json([
            'success' => 'Due Date has been Updated.',
            'date' => Carbon::create(request()->due_date)->format('F j, Y')
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        $this->authorize('delete', $todo->project);
        
        $todo->delete();

        return response()->json([
            'success' => 'Todo has been Deleted.'
        ], 200);
    }

    public function invite(Todo $todo)
    {
        if (request()->value == true) {
            return $this->attach($todo, request()->all());
        }


        return $this->detach( $todo, request()->all() );
    }

    public function attach($todo, $request)
    {
        $invited = $this->inviteUser($todo, $request, 'todo');

        return response()->json([
            $invited['type'] => $invited['message'],
            'employees' => UserResource::collection($todo->fresh()->employees)
        ], $invited['status']);
    }

    public function detach($todo, $request)
    {
        $user = User::whereEmail($request['email'])->first();

        $todo->employees()->detach($user);

        return response()->json([
            'success' => 'User has been removed from Todo.',
            'employees' => UserResource::collection($todo->fresh()->employees)
        ], 200);

    }
}
