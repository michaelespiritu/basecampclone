<?php

namespace App\Http\Controllers\Project;

use App\User;
use App\Model\Project;
use App\Traits\Invite;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\ProjectResource;

class ProjectController extends Controller
{

    use Invite;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProjectResource::collection(auth()->user()->project()->orderBy('created_at', 'desc')->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate(['name' => 'required']);

        $project = auth()->user()->project()->create(
            [   
                'identifier' => Str::uuid(),
                'member_id' => json_encode(array(auth()->user()->id)),
                'name' => ucfirst(request()->name),
                'description' => request()->description
            ]
        );


        return response()->json([
            'success' => 'Project has been Created.',
            'identifier' => $project->identifier
        ], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return view('projects.view')
            ->with('project', $project)
            ->with('members', UserResource::collection($project->employees))
            ->with('company', CompanyResource::make($project->owner->ownCompany));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $this->authorize('update', $project);

        return view('projects.edit')->with('project', $project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project)
    {
        $this->authorize('update', $project);

        request()->validate(['name' => 'required']);

        $project->update([
            'name' => ucfirst(request()->name),
            'description' => request()->description
        ]);

        return response()->json([
            'success' => 'Project has been Updated.',
            'identifier' => $project->identifier
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $this->authorize('delete', $project);
        
        $project->delete();

        return response()->json([
            'success' => 'Project has been Deleted.',
        ], 200);
    }


    public function invite(Project $project)
    {
        if (request()->value == true) {
            return $this->attach($project, request()->all());
        }


        return $this->detach( $project, request()->all() );
    }

    public function attach($project, $request)
    {
        $invited = $this->inviteUser($project, $request, 'project');

        return response()->json([
            $invited['type'] => $invited['message'],
            'employees' => UserResource::collection($project->fresh()->employees)
        ], $invited['status']);
    }

    public function detach($project, $request)
    {
        $user = User::whereEmail($request['email'])->first();

        $project->employees()->detach($user);

        return response()->json([
            'success' => 'User has been removed from Project.',
            'employees' => UserResource::collection($project->fresh()->employees)
        ], 200);

    }
}
