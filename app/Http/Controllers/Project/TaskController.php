<?php

namespace App\Http\Controllers\Project;

use Carbon\Carbon;
use App\Model\Task;
use App\Model\Todo;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Todo $todo)
    {
        foreach (request()->tasks as $task) {
            $todo->task()->create([
                'identifier' => Str::uuid(),
                'task' => ucfirst($task['details']),
                'done' => null,
                'done_by' => null
            ]);
        }

        return response()->json([
            'success' => 'Task has been Created.',
            'task' => $todo->task
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDone(Task $task)
    {
        $task->update([
            'done' => (request()->done === true) ? Carbon::now() : null,
            'done_by' => (request()->done === true) ? auth()->user()->id : null
        ]);

        return response()->json([
            'success' => 'Task has been Updated',
            'task' => $task->todo->task
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
