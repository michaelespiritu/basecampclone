<?php

namespace App\Http\Controllers\Web;

use App\User;
use App\Model\Company;
use App\Traits\Invite;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;

class CompanyController extends Controller
{

    use Invite;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // return public_path().'/fonts/Raleway.ttf';
        return view('web.company', 
                    ['company' => (auth()->user()->ownCompany) 
                    ? CompanyResource::make(auth()->user()->ownCompany) 
                    : ['name' => null,'path' => '/company']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $count = count(auth()->user()->ownCompany);

        request()->validate(['name' => 'required']);
        
        if ($count <= 0) {
            
            $company = auth()->user()->ownCompany()->create([
                'identifier' => Str::uuid(),
                'name' => ucfirst(request()->name)
            ]);

            return response()->json([
                'success' => 'Company has been created.',
                'company' => CompanyResource::make($company)
            ], 200);

        }

        return response()->json([
            'error' => 'You are not allowed to create more than 1 company.',
        ], 403);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Company $company)
    {
        request()->validate(['name' => 'required']);
        
        $company->update([
            'name' => ucfirst(request()->name)
        ]);

        return response()->json([
            'success' => 'Company has been Updated.',
            'company' => CompanyResource::make($company->fresh())
        ], 200);

    }

    public function invite(Company $company)
    {
        $invited = $this->inviteUser($company, request()->all(), 'company');

        return response()->json([
            $invited['type'] => $invited['message'],
            'company' => CompanyResource::make($company->fresh())
        ], $invited['status']);

    }


    public function detach(Company $company)
    {
        $user = User::whereEmail(request()->email)->first();

        if (request()->where == 'all') {
            foreach($company->owner->project as $project) {
                $project->employees()->detach($user);
            }  
    
            $company->employees()->detach($user);
    
            return response()->json([
                'success' => 'User has been removed from your company, project and todo.',
                'company' => CompanyResource::make($company->fresh())
            ], 200);
        }


        if (request()->where == 'project') {
            foreach($company->owner->project as $project) {
                $project->employees()->detach($user);
            }  
    
            return response()->json([
                'success' => 'User has been removed from your Projects.',
                'company' => CompanyResource::make($company->fresh())
            ], 200);
        }

    }
}
