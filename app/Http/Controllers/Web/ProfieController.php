<?php

namespace App\Http\Controllers\Web;

use App\User;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;

class ProfieController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web.profile', ['profile' => UserResource::make(auth()->user())]);
    }


    /**
     * Store a newly created resource in storage.
     *
     */
    public function store()
    {
        $user = auth()->user();

        $profile = $user->profile()->updateOrCreate(
            [   
                'user_id' => auth()->user()->id
            ],
            [
                'timezone' => request()->timezone,
                'profile_image' => null,
            ]
        )->first();

        $user->update([
            'name' => Str::title(request()->name)
        ]);

        return $user;
    }


    /**
     * Update Email of specific resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateEmail()
    {
        $validation = $this->emailValidation(request()->new_email, request()->old_email);

        if ($validation) {
            return $validation;
        }
        
        $user = auth()->user()->update([
            'email' => request()->new_email
        ]);

        return response()->json([
            'success' => 'Email has been updated',
            'user' => $user
        ], 200);
    }


    /**
     * Validate Email of specific resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function emailValidation($newEmail, $oldEmail)
    {
        $user = User::where('email', $newEmail)->first();

        if ($user) {
            return response()->json([
                'error' => 'Email is already registered'
            ], 403);
        }


        if (auth()->user()->email != $oldEmail) {
            return response()->json([
                'error' => 'Old Email is incorrect'
            ], 403);
        }

        return false;
    }


    /**
     * Update Password of specific resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePassword()
    {
        $validation = $this->passwordValidation(request()->new_password, request()->old_password);

        if ($validation) {
            return $validation;
        }
        
        $user = auth()->user()->update([
            'password' => Hash::make(request()->new_password)
        ]);

        return response()->json([
            'success' => 'Password has been updated',
            'user' => $user
        ], 200);
    }


    /**
     * Validate Password of specific resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function passwordValidation($newPassword, $oldPassword)
    {
        if (Hash::check($newPassword, auth()->user()->password)) {
            return response()->json([
                'error' => 'You can\'t use your old Password'
            ], 403);
        }

        if (!Hash::check($oldPassword, auth()->user()->password)) {
            return response()->json([
                'error' => 'Old Password Incorrect'
            ], 403);
        }

        return false;
    }
}
