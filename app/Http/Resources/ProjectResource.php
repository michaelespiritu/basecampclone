<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'identifier' => $this->identifier,
            'project_name' => $this->name,
            'project_description' => $this->description,
            'created_at' => $this->created_at->format('F j, Y'),
            'todo_count' => count($this->todo->where('status', 'todo')),
            'doing_count' => count($this->todo->where('status', 'doing')),
            'done_count' => count($this->todo->where('status', 'done'))
        ];
    }
}
