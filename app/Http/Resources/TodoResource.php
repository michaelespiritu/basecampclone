<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TodoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'identifier' => $this->identifier,
            'order' => $this->id,
            'todo_name' => $this->name,
            'todo_description' => $this->description,
            'created_at' => $this->created_at->format('F j, Y'),
            'due_date' => $this->due_date->format('F j, Y'),
            'status' => $this->status,
            'assigned_to' => UserResource::collection($this->employees)
        ];
    }
}
